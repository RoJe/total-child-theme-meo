<?php
/**
 * Child theme functions
 *
 * When using a child theme (see http://codex.wordpress.org/Theme_Development
 * and http://codex.wordpress.org/Child_Themes), you can override certain
 * functions (those wrapped in a function_exists() call) by defining them first
 * in your child theme's functions.php file. The child theme's functions.php
 * file is included before the parent theme's file, so the child theme
 * functions would be used.
 *
 * Text Domain: total
 * @link http://codex.wordpress.org/Plugin_API
 *
 */

function total_child_enqueue_parent_theme_style() {

	// Dynamically get version number of the parent stylesheet (lets browsers re-cache your stylesheet when you update your theme)
	$theme   = wp_get_theme( 'Total' );
	$version = $theme->get( 'Version' );

	// Load the stylesheet
	wp_enqueue_style( 'parent-style', get_template_directory_uri().'/style.css', array(), $version );
	
}
add_action( 'wp_enqueue_scripts', 'total_child_enqueue_parent_theme_style' );


add_filter( 'bp_ajax_querystring', 'sort_alpha_by_default', 32, 2 );
function sort_alpha_by_default( $query_string, $object ) {
        if ( 'members' == $object ) {
        
                $query_args = wp_parse_args( $query_string, array() );
        
                if ( empty( $query_args['type'] ) ) {
                
                        $query_args['type'] = 'alphabetical';           
                        $query_string = http_build_query( $query_args );
                }
        }

        return $query_string;
}